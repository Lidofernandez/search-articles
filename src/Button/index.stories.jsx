import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Button from "./index";

const ButtonStory = ({ ...props }) => <Button {...props} icon="close" />;

storiesOf("Button", module)
  .add("With icon", () => <ButtonStory onClick={action("button-click")} />)
  .add("With focus", () => <ButtonStory autoFocus />);
