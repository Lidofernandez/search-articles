import React from "react";
import PropTypes from "prop-types";

import Icon from "../Icon";

import "./index.css";

const Button = ({ type, icon, className, children, ...props }) => (
  // eslint-disable-next-line react/button-has-type
  <button
    type={type}
    className={`button ${icon ? "button--has-icon" : ""} ${className}`}
    {...props}
  >
    {icon ? <Icon icon={icon} /> : children}
  </button>
);

Button.propTypes = {
  /** sets a button type */
  type: PropTypes.string,
  /** sets a trailing icon name */
  icon: PropTypes.string,
  /**  sets class name */
  className: PropTypes.string,
  /** renders a children */
  children: PropTypes.node,
};

Button.defaultProps = {
  type: "button",
  icon: undefined,
  className: "",
  children: undefined,
};

export default Button;
