import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import "./index.css";

import highligthString from "../helpers/highligthString";

class Suggestions extends PureComponent {
  renderList() {
    const { suggestions, highlightSuggestion, onClick } = this.props;
    const list = suggestions.map(({ searchterm, nrResults }) => {
      const suggestion = highligthString(searchterm, highlightSuggestion);
      return (
        <li
          role="option"
          aria-selected={false}
          key={searchterm}
          data-testid={searchterm}
          onKeyDown={() => onClick(searchterm)}
          onClick={() => onClick(searchterm)}
          className="suggestion"
        >
          {suggestion} {nrResults && `(${nrResults})`}
        </li>
      );
    });

    return list;
  }

  render() {
    return (
      <ul role="listBox" className="suggestions">
        {this.renderList()}
      </ul>
    );
  }
}

Suggestions.propTypes = {
  /** loads a suggestion list */
  suggestions: PropTypes.arrayOf(PropTypes.object),
  /** sets a highlited value */
  highlightSuggestion: PropTypes.string,
  /** sets an event handler */
  onClick: PropTypes.func,
};

Suggestions.defaultProps = {
  suggestions: [],
  highlightSuggestion: undefined,
  onClick() {},
};

export default Suggestions;
