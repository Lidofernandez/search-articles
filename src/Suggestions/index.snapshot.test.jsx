import React from "react";
import { render } from "react-testing-library";

import Suggestions from "./index";

describe("Suggestions", () => {
  it("should be rendered", () => {
    const { container } = render(
      <Suggestions
        suggestions={[
          { searchterm: "Chuck Norris" },
          { searchterm: "Bruce Lee" },
        ]}
      />,
    );

    expect(container.firstChild).toMatchSnapshot();
  });
});
