import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Suggestions from "./index";

const SuggestionsStory = ({ ...props }) => (
  <Suggestions
    {...props}
    suggestions={[{ searchterm: "Chuck Norris" }, { searchterm: "Bruce Lee" }]}
  />
);

storiesOf("Suggestions", module)
  .add("Simple", () => <SuggestionsStory />)
  .add("With highlight Suggestion", () => (
    <SuggestionsStory highlightSuggestion="Bruce" />
  ))
  .add("With onClick event", () => (
    <SuggestionsStory highlightSuggestion="Bruce" onClick={action("click")} />
  ));
