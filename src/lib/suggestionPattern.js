export default suggestion => new RegExp(suggestion, "i");
