import React, { PureComponent } from "react";

import SearchBox from "./SearchBox";
import getArticles from "./getArticles";
import suggestionPattern from "./lib/suggestionPattern";
import Button from "./Button";
import TextField from "./TextField";
// This could be set as an enviroment variable
// so I don't have to make an extra commit to update
// this value
const MINIMUN_VALUE = 2;

// I've decided to keep this component only for the props
// related with page transitions or initial setups

class App extends PureComponent {
  state = {
    filteredArticles: [],
  };

  handleOnSubmit = async result => {
    const { articles } = await getArticles(result);
    const filteredArticles = articles.filter(({ name }) =>
      // As I don't access to query to a real db this is how
      // I fixed it
      suggestionPattern(result).test(name),
    );
    this.setState({ filteredArticles });
  };

  // handleOnDelete = () => {
  // Here I would include a Post Method to delete the article
  // based on id
  // };

  // handleOnAdd = () => {
  // Here I would include a Post Method to add a new article
  // on submit
  // };

  renderArticles() {
    const { filteredArticles } = this.state;

    const list = filteredArticles.map(({ name, description }) => (
      <li className="li" data-testid={name} key={name}>
        {/* Here I would implement a similar form as I did for the search
            with a onSubmit={this.handleOnDelete} for example
          */}
        {name} - {description} <Button icon="delete" />
      </li>
    ));

    return <ul data-testid="resuls">{list}</ul>;
  }

  render() {
    return (
      <div className="container">
        <div className="content">
          <SearchBox
            onSubmit={this.handleOnSubmit}
            minimumSearchValue={MINIMUN_VALUE}
            placeholder="Zoeken"
          />
          {this.renderArticles()}
          {/* Here I would implement a similar form as I did for the search
              with a onSubmit={this.handleOnAdd} for example
            */}
          <form className="form--container">
            <TextField placeholder="Title" />
            <TextField placeholder="Description" />
            <Button icon="add" />
          </form>
        </div>
      </div>
    );
  }
}
export default App;
