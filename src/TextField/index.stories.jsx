import React from "react";
import { storiesOf } from "@storybook/react";

import TextField from "./index";
import Icon from "../Icon";
import Button from "../Button";

const TextFieldStory = ({ ...props }) => (
  <TextField {...props} placeholder="text field" />
);

storiesOf("TextField", module)
  .add("Simple", () => <TextFieldStory />)
  .add("With value", () => <TextFieldStory value="field" />)
  .add("With focus", () => <TextFieldStory focused />)
  .add("With icon", () => <TextFieldStory withIcon="delete" />)
  .add("With icon component", () => (
    <TextFieldStory withIcon={<Icon icon="email" />} />
  ))
  .add("With button icon component", () => (
    <TextFieldStory withIcon={<Button icon="delete" />} />
  ));
