import React from "react";
import { render, cleanup } from "react-testing-library";

import TextField from "./index";

afterEach(cleanup);

describe("TextField", () => {
  describe("With icon", () => {
    it("should render the icon", () => {
      const { getByTestId } = render(<TextField withIcon="clear" />);

      const icon = getByTestId("icon");
      expect(icon).toHaveTextContent("clear");
    });
    it("should render the icon component", () => {
      const { container } = render(<TextField withIcon={<span>test</span>} />);

      expect(container).toHaveTextContent("test");
    });
  });
  describe("With focus", () => {
    it("should add the className is focus", () => {
      const { container } = render(<TextField focused />);

      const hasActiveClass = container.querySelector(".text-field");
      expect(hasActiveClass.className).toContain("is-focused");
    });
  });
});
