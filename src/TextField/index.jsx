import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import Icon from "../Icon";

import "./index.css";

class TextField extends PureComponent {
  renderInput() {
    const {
      value,
      onChange,
      onFocus,
      onBlur,
      onKeyUp,
      placeholder,
      name,
    } = this.props;

    return (
      <input
        type="text"
        className="text-field__input"
        placeholder={placeholder}
        aria-label={placeholder}
        name={name}
        onChange={onChange}
        value={value}
        onFocus={onFocus}
        onBlur={onBlur}
        onKeyUp={onKeyUp}
        aria-autocomplete="list"
      />
    );
  }

  render() {
    const { focused, children, withIcon } = this.props;
    const iconIsString =
      // small test to check the string type
      withIcon && typeof withIcon === "string";

    return (
      <div
        className={`text-field ${withIcon ? "text-field__has-icon" : ""}${
          focused ? " is-focused" : ""
        }`}
      >
        {this.renderInput()}
        {iconIsString ? <Icon data-testid="icon" icon={withIcon} /> : withIcon}
        {children}
      </div>
    );
  }
}

TextField.propTypes = {
  /** sets the value */
  value: PropTypes.string,
  /** sets the focused state */
  focused: PropTypes.bool,
  /** renders a children */
  children: PropTypes.node,
  /** sets an event handler */
  onChange: PropTypes.func,
  /** sets an event handler */
  onFocus: PropTypes.func,
  /** sets an event handler */
  onBlur: PropTypes.func,
  /** sets an event handler */
  onKeyUp: PropTypes.func,
  /** sets the placeholder */
  placeholder: PropTypes.string,
  /** sets the name */
  name: PropTypes.string,
  /** renders an icon */
  withIcon: PropTypes.node,
};

TextField.defaultProps = {
  value: undefined,
  focused: false,
  children: undefined,
  onChange() {},
  onFocus() {},
  onBlur() {},
  onKeyUp() {},
  placeholder: undefined,
  name: undefined,
  withIcon: undefined,
};

export default TextField;
