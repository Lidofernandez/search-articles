import React from "react";
import { render } from "react-testing-library";

import TextField from "./index";

const props = {
  placeholder: "Test",
};

describe("TextField", () => {
  it("should be rendered", () => {
    const { container } = render(<TextField {...props} />);

    expect(container.firstChild).toMatchSnapshot();
  });
  describe("With Icon", () => {
    it("should be rendered", () => {
      const { container } = render(<TextField {...props} withIcon="clear" />);

      expect(container.firstChild).toMatchSnapshot();
    });
  });
});
