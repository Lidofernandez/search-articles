import React from "react";
import { render } from "react-testing-library";

import Icon from "./index";

describe("Icon", () => {
  it("should be rendered", () => {
    const { container } = render(<Icon icon="test" />);

    expect(container.firstChild).toMatchSnapshot();
  });
});
