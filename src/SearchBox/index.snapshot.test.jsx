import React from "react";
import { render } from "react-testing-library";

import SearchBox from "./index";

describe("SearchBox", () => {
  it("should be rendered", () => {
    const { container } = render(<SearchBox />);

    expect(container.firstChild).toMatchSnapshot();
  });
});
