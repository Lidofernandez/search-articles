export default query =>
  fetch(`http://localhost:3000/search?q=${query}`)
    .then(response => response.json())
    .then(data => data);
