import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import SearchBox from "./index";

storiesOf("SearchBox", module)
  .add("Simple", () => (
    <SearchBox placeholder="Search" onSubmit={action("form-submit")} />
  ))
  .add("With minimum search value", () => (
    <SearchBox
      placeholder="Search"
      minimumSearchValue={1}
      onSubmit={action("form-submit")}
    />
  ));
