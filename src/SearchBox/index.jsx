import React, { PureComponent, Fragment } from "react";
import PropTypes from "prop-types";

import Button from "../Button";
import Suggestions from "../Suggestions";
import TextField from "../TextField";

import getSuggestions from "./getSuggestions";

import suggestionPattern from "../lib/suggestionPattern";

import "./index.css";

class SearchBox extends PureComponent {
  state = {
    value: "",
    focused: false,
    showSuggestions: false,
    filteredSuggestions: [],
  };

  handleOnChange = ({ target }) => {
    this.setState({
      value: target.value,
    });
  };

  handleOnClear = () => {
    this.setState({
      value: "",
      filteredSuggestions: [],
      showSuggestions: false,
    });
  };

  handleOnSubmit = event => {
    event.preventDefault();
    const { onSubmit } = this.props;
    const { value } = this.state;
    this.setState({ showSuggestions: false });
    onSubmit(value);
  };

  handleOnFocus = () => {
    this.setState({
      focused: true,
    });
  };

  handleOnBlur = () => {
    this.setState({
      focused: false,
    });
  };

  // As per the current requirements I've decided not
  // to include a debounce method or throttle
  // and keep it KISS, but as the next feature it will
  // be nessesary
  handleOnKeyUp = async () => {
    const { value } = this.state;
    const { minimumSearchValue } = this.props;
    if (value.length <= minimumSearchValue) {
      this.setState({
        showSuggestions: false,
      });
      return;
    }

    const { suggestions } = await getSuggestions(value);
    const filteredSuggestions = suggestions.filter(({ searchterm }) =>
      suggestionPattern(value).test(searchterm),
    );
    this.setState({
      filteredSuggestions,
      showSuggestions: true,
    });
  };

  handleOnSelectValue = value => {
    this.setState({
      value,
      showSuggestions: false,
    });
  };

  suggestions() {
    const { filteredSuggestions, value, showSuggestions } = this.state;
    if (!showSuggestions) {
      return null;
    }

    if (value && !filteredSuggestions.length) {
      return (
        <span data-testid="notFound">
          {`There are no results for: ${value}`}
        </span>
      );
    }

    return (
      <Suggestions
        suggestions={filteredSuggestions}
        highlightSuggestion={value}
        onClick={this.handleOnSelectValue}
      />
    );
  }

  renderTextField() {
    const { value, focused } = this.state;
    const { placeholder, name } = this.props;
    // As per the requirements I'm assuming that the search
    // needs a clear button and a submit button to get
    // the selected query
    return (
      <TextField
        placeholder={placeholder}
        name={name}
        onChange={this.handleOnChange}
        value={value}
        onFocus={this.handleOnFocus}
        onBlur={this.handleOnBlur}
        onKeyUp={this.handleOnKeyUp}
        focused={focused}
        withIcon={
          <Fragment>
            {value && (
              <Button
                data-testid="clear"
                onClick={this.handleOnClear}
                tabIndex="0"
                icon="clear"
                aria-label="annuleren"
              />
            )}
            <Button
              type="submit"
              data-testid="submit"
              tabIndex="0"
              icon="search"
              className="form__submit-icon"
              aria-label="zoeken"
            />
          </Fragment>
        }
      />
    );
  }

  render() {
    return (
      <form className="form" onSubmit={this.handleOnSubmit} role="search">
        {this.renderTextField()}
        {this.suggestions()}
      </form>
    );
  }
}

SearchBox.propTypes = {
  /** OnSubmit will trigger the form */
  onSubmit: PropTypes.func,
  /** The minimun value in which the search will start giving suggestions */
  minimumSearchValue: PropTypes.number,
  /**
    Placeholder for the text field
  */
  placeholder: PropTypes.string,
  /** Text field name */
  name: PropTypes.string,
};

SearchBox.defaultProps = {
  onSubmit() {},
  minimumSearchValue: 0,
  placeholder: undefined,
  name: undefined,
};

export default SearchBox;
