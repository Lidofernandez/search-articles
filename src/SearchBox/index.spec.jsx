import React from "react";
import { render, fireEvent, waitForElement } from "react-testing-library";

import SearchBox from "./index";

const handlOnChange = value => ({
  target: { value },
});

const props = {
  placeholder: "Zoeken",
};

describe("SearchBox", () => {
  it("should submit the form", () => {
    const onSubmit = jest.fn();
    const { getByTestId } = render(<SearchBox onSubmit={onSubmit} />);

    fireEvent.click(getByTestId("submit"));
    expect(onSubmit).toHaveBeenCalledTimes(1);
  });

  describe("With clear button", () => {
    it("should show the icon when there is a value", () => {
      const { getByTestId, getByPlaceholderText } = render(
        <SearchBox {...props} />,
      );
      const search = getByPlaceholderText("Zoeken");

      fireEvent.change(search, handlOnChange("test"));
      const clearButton = getByTestId("clear");
      expect(clearButton).toHaveTextContent("clear");
    });
    it("should remove the value on click", () => {
      const { getByTestId, getByPlaceholderText } = render(
        <SearchBox {...props} />,
      );
      const search = getByPlaceholderText("Zoeken");

      fireEvent.change(search, handlOnChange("test"));
      fireEvent.click(getByTestId("clear"));
      expect(search.value).toEqual("");
    });
  });

  describe("With focus", () => {
    it("should add the className on focus", () => {
      const { getByPlaceholderText, container } = render(
        <SearchBox {...props} />,
      );
      const search = getByPlaceholderText("Zoeken");

      fireEvent.focus(search);
      const hasActiveClass = container.querySelector(".text-field");
      expect(hasActiveClass.className).toContain("is-focused");
    });
    it("should remove the className on blur", () => {
      const { getByPlaceholderText, container } = render(
        <SearchBox {...props} />,
      );
      const search = getByPlaceholderText("Zoeken");

      fireEvent.blur(search);
      const hasActiveClass = container.querySelector(".text-field");
      expect(hasActiveClass.className).not.toContain("is-focused");
    });
  });

  beforeEach(() => {
    global.fetch = jest.fn().mockImplementation(
      () =>
        new Promise(resolve =>
          resolve({
            json() {
              return {
                suggestions: [
                  { searchterm: "Chuck Norris" },
                  { searchterm: "Antonio Banderas" },
                ],
              };
            },
          }),
        ),
    );
  });

  describe("With suggestions", () => {
    it("should update the value once clicked on any list item", async () => {
      const { getByPlaceholderText, getByTestId } = render(
        <SearchBox {...props} />,
      );
      const search = getByPlaceholderText("Zoeken");
      const inputText = "Chu";
      fireEvent.change(search, handlOnChange(inputText));

      [...inputText].forEach(text => {
        fireEvent.keyUp(search, { key: text });
      });
      await waitForElement(() => getByTestId("Chuck Norris"));
      fireEvent.click(getByTestId("Chuck Norris"));
      expect(search.value).toEqual("Chuck Norris");
    });
    it("should render the list", async () => {
      const { getByPlaceholderText, getByTestId } = render(
        <SearchBox {...props} />,
      );
      const search = getByPlaceholderText("Zoeken");
      const inputText = "Chu";
      fireEvent.change(search, handlOnChange(inputText));

      [...inputText].forEach(text => {
        fireEvent.keyUp(search, { key: text });
      });
      const suggestion = await waitForElement(() =>
        getByTestId("Chuck Norris"),
      );
      expect(suggestion).toHaveTextContent("Chuck Norris");
    });
    it("should render a message when there are no resuls", async () => {
      const { getByPlaceholderText, getByTestId } = render(
        <SearchBox {...props} />,
      );
      const search = getByPlaceholderText("Zoeken");
      const inputText = "Bruce";
      fireEvent.change(search, handlOnChange(inputText));

      [...inputText].forEach(text => {
        fireEvent.keyUp(search, { key: text });
      });
      const suggestion = await waitForElement(() => getByTestId("notFound"));
      expect(suggestion).toHaveTextContent("There are no results for: Bruce");
    });
    it("should remove suggestions when clearing the input", async () => {
      const { getByPlaceholderText, getByTestId, container } = render(
        <SearchBox {...props} />,
      );
      const search = getByPlaceholderText("Zoeken");
      const inputText = "Chu";
      fireEvent.change(search, handlOnChange(inputText));

      [...inputText].forEach(text => {
        fireEvent.keyUp(search, { key: text });
      });
      await waitForElement(() => getByTestId("Chuck Norris"));
      fireEvent.click(getByTestId("clear"));
      const hasSuggestions = container.querySelector(".suggestions");
      expect(search.value).toEqual("");
      expect(hasSuggestions).toBe(null);
    });
    it("should hide the list when the form was submitted", () => {
      const onSubmit = jest.fn();
      const { getByTestId, container } = render(
        <SearchBox onSubmit={onSubmit} {...props} />,
      );
      fireEvent.click(getByTestId("submit"));
      const hasSuggestions = container.querySelector(".suggestions");
      expect(onSubmit).toHaveBeenCalledTimes(1);
      expect(hasSuggestions).toBe(null);
    });
  });

  describe("With minimum search value", () => {
    it("should render the suggestions", async () => {
      const { getByPlaceholderText, getByTestId } = render(
        <SearchBox minimumSearchValue={4} {...props} />,
      );
      const search = getByPlaceholderText("Zoeken");
      const inputText = "Chuck";
      fireEvent.change(search, handlOnChange(inputText));

      [...inputText].forEach(text => {
        fireEvent.keyUp(search, { key: text });
      });
      const suggestion = await waitForElement(() =>
        getByTestId("Chuck Norris"),
      );
      expect(suggestion).toHaveTextContent("Chuck Norris");
    });
  });
});
