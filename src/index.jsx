import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

/*
  As per the requirements I've decided to fork/merge my own
  repository for the initial project setup, in terms of formatting, linting,
  and testing javascript files. I also configured a small pipeline
  for continuos integration, and added a11y eslint plugin for accesibilty.

  For the testing strategy I've used snapshots and the react-testing-library.
  The reason behind is that Snapshot tests doesn't really allow TDD,
  but it's very usefull for kepping the layout consistent and check if a change
  has broken other components, plus building all the DOM can make the tests
  a bit slower.

  I've used the react-testing-library over Enzyme because it avoids to use
  bad practices like trying to test react internals, for instance if a
  component has a specific prop or state, plus I've never found Shallow
  to be really useful for unit testing. react-testing-library on the other
  hand is smaller and more intuitive about how the user is going to interact
  with the application.

  So, in orther to have the best of the both libraries I've added two scripts
  one for running the unit testing with react-testing-library and another one
  for the snapshot testing.

  Finally, to check if all the components are tested I've added the jest
  coverage script.
*/
ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
