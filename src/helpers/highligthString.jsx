import React from "react";

import "./highligthString.css";

import suggestionPattern from "../lib/suggestionPattern";

export default (string, highlight) => {
  if (!highlight) {
    return string;
  }
  const pattern = suggestionPattern(highlight);
  const splitString = string.split(pattern);
  if (splitString.length <= 1) {
    return string;
  }

  const matches = string.match(pattern);
  return (
    <span className="highlight">
      {splitString.reduce((previous, current, index) => {
        const matchedIndex = matches[index];
        if (!matchedIndex) {
          return [...previous, current];
        }

        return previous.concat(
          ...previous,
          current,
          <span key={`${matchedIndex}${current}`} className="isHighlighted">
            {matchedIndex}
          </span>,
        );
      }, [])}
    </span>
  );
};
