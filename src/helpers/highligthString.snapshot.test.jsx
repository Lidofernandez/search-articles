import highligthString from "./highligthString";

describe("highligthString", () => {
  it("should be rendered", () => {
    const highligthed = highligthString("Chuck Norris", "Chuck");
    expect(highligthed).toMatchSnapshot();
  });
  describe("When there is no value to highlight", () => {
    it("should return the string", () => {
      const highligthed = highligthString("Chuck Norris");
      expect(highligthed).toMatchSnapshot();
    });
  });
});
