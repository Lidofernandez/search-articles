import { render } from "react-testing-library";

import highligthString from "./highligthString";

describe("highligthString", () => {
  it("should highligth the value", () => {
    const { container } = render(highligthString("Chuck Norries", "Chuck"));
    const isHighlighted = container.querySelector(".isHighlighted");
    expect(isHighlighted).toHaveTextContent("Chuck");
  });
  it("should highlight the value with different letter case", () => {
    const { container } = render(highligthString("Chuck Norries", "Norries"));
    const isHighlighted = container.querySelector(".isHighlighted");
    expect(isHighlighted).toHaveTextContent("Norries");
  });
  it("should not change the highlighted value", () => {
    const { container } = render(
      highligthString("Chuck Norries forever", "Forever"),
    );
    const isHighlighted = container.querySelector(".isHighlighted");
    expect(isHighlighted).toHaveTextContent("forever");
  });
});
