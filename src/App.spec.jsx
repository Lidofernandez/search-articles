import ReactDOM from "react-dom";
import React from "react";
import { render, fireEvent, waitForElement } from "react-testing-library";

import App from "./App";

const handlOnChange = value => ({
  target: { value },
});

describe("App", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<App />, div);
  });
  beforeEach(() => {
    global.fetch = jest.fn().mockImplementation(
      () =>
        new Promise(resolve =>
          resolve({
            json() {
              return {
                articles: [
                  { name: "Chuck Norris", description: "Is the best" },
                  { name: "Antonio Banderas", description: "Could be better" },
                ],
              };
            },
          }),
        ),
    );
  });
  it("should render a list by article", async () => {
    const { getByTestId, getByPlaceholderText } = render(<App />);
    const search = getByPlaceholderText("Zoeken");
    const inputText = "Chuck Norris";
    fireEvent.change(search, handlOnChange(inputText));
    fireEvent.click(getByTestId("submit"));
    const articleName = await waitForElement(() => getByTestId("Chuck Norris"));
    expect(articleName).toHaveTextContent("Is the best");
  });
});
